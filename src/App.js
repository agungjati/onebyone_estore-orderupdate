import React, { Component } from 'react';
import { Container, Row, Col, Button, Form, Label, Input, Table }
  from 'reactstrap'
import PropTypes from 'prop-types';

class App extends Component {
  render() {
    return (
      <Container className="bg-light">
        <Row className="bg-white shadow">
          <Col md={{ size: 12 }} className="border-bottom py-2"><h4 className="text-center">eStore Logistics Order Update</h4></Col>
          <Col md={{ size: 12 }} className="p-3">
            <Form>
              <Row>
                <Col md={{ size: 3 }}>
                  <Label><b>Enter Shipment ID :</b></Label>
                </Col>
                <Col md={{ size: 6 }} className="d-flex justify-content-between">
                  <Input type="text" placeholder="ERP_ORDER" />
                  <Button className="ml-2">RETRIEVE</Button>
                </Col>
              </Row>
            </Form>
            <Row className="mt-3">
              <Col md={{ size: 9 }}>
                <b >List of shipments (This would be a nested table and if multiple shipment exist, a particular shipment can be clicked to populate the details in the next table)</b>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={{ size: 12 }}>
                <Table size="sm" bordered responsive>
                  <thead>
                    <tr>
                      <th>DATE CREATED</th>
                      <th>ERP_ORDER</th>
                      <th>COSTUMER NAME</th>
                      <th>SUBURB</th>
                      <th>POSTCODE</th>
                      <th>STATE</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>SKU</td>
                      <td>SKU DESC</td>
                      <td>QTY</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>SKU</td>
                      <td>SKU DESC</td>
                      <td>QTY</td>
                    </tr>
                    <tr>
                      <td>DATE CREATED</td>
                      <td>ERP_ORDER</td>
                      <td>COSTUMER NAME</td>
                      <td>SUBURB</td>
                      <td>POSTCODE</td>
                      <td>STATE</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>SKU</td>
                      <td>SKU DESC</td>
                      <td>QTY</td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={{ size: 12}} >
                <h4>Shipment Detail</h4>
                <Table size="sm" bordered className="mt-1" responsive>
                  <tbody>
                    <tr>
                      <td>NAME</td>
                      <td>&nbsp;</td>
                      <td>EMAIL</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>ADDRESS1</td>
                      <td>&nbsp;</td>
                      <td>PHONE</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>ADDRESS2</td>
                      <td>&nbsp;</td>
                      <td>COUNTRY</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>CITY</td>
                      <td>&nbsp;</td>
                      <td>COSTUMER CATEGORY</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>STATE</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>SKU</td>
                      <td>SKU DESC</td>
                      <td>QTY</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>SKU</td>
                      <td>SKU DESC</td>
                      <td>QTY</td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
            <Row>
              <Col md={{ size: 3, offset: 9 }}>
                <Button>UPDATE</Button>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={{ size: 3, offset: 9 }}>
                <Button>DELETE</Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

Table.propTypes = {
  size: PropTypes.string,
  bordered: PropTypes.bool,
  responsive: PropTypes.bool
}

const stringOrNumberProp = PropTypes.oneOfType([PropTypes.number, PropTypes.string]);
const columnProps = PropTypes.oneOfType([
  PropTypes.shape({
    size: PropTypes.oneOfType([PropTypes.bool, PropTypes.number, PropTypes.string]),
    offset: stringOrNumberProp
  })
]);

Col.propTypes = {
  md: columnProps
}

export default App;