import React, { Component } from 'react'
import {
    Button, Input, Label, Form,
    Container, Col, Row
} from 'reactstrap'
import { HeaderApi, UrlApi } from '../../headers'
import axios from 'axios'
import { Link} from 'react-router-dom'

class Ship extends Component {
    constructor(props) {
        super(props)

        this.state = {
            company: 0,
            erp_order: 0,
            ship_to_name: "",
            ship_to_address1: "",
            ship_to_address2: "",
            ship_to_city: "",
            ship_to_state: "",
            ship_to_postal_code: "",
            ship_to_emailaddress: "",
            ship_to_phonenum: ""
        }
    }

    handleChange = event => {
        
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    validateForm() {
        const { company, erp_order, ship_to_name, ship_to_address1,
            ship_to_address2, ship_to_city, ship_to_state, ship_to_postal_code
         } = this.state

         return ( company.length > 0 && erp_order.length > 0 && ship_to_name.length > 0 && 
            ship_to_address1.length > 0 && ship_to_address2.length > 0 && ship_to_city.length > 0 
            && ship_to_state.length > 0 && ship_to_postal_code.length > 0 )
    }

    handleSubmit = event => {
        console.log(this.state)
        event.preventDefault()
        axios({
            method: "POST",
            url: `${UrlApi}/update_order`,
            data: JSON.stringify(this.state),
            headers: HeaderApi
        }).then(response => {
            console.log(response)
           
        })
        
        
    }

    render() {
        const { company, erp_order, ship_to_name, ship_to_address1,
            ship_to_address2, ship_to_city, ship_to_state, ship_to_postal_code,
            ship_to_emailaddress, ship_to_phonenum
         } = this.state

        return (
            <Container className="mt-3">
                <Row>
                    <Col md={{ size: 8, offset: 2 }} className="shadow py-3">
                        <Link to="/">Back</Link>
                        <Form onSubmit={this.handleSubmit}>
                            <Row>
                                <Col sm={{ size: 3 }}>
                                    <Label>Company :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="text" id="company" value={company} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>ERP Order :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="numbert" id="erp_order" value={erp_order} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>Name :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="text" id="ship_to_name" value={ship_to_name} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>Addres 1 :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="text" id="ship_to_address1" value={ship_to_address1} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>Addres 2 :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="text" id="ship_to_address2" value={ship_to_address2} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>City :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="text" id="ship_to_city" value={ship_to_city} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>State :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="text" id="ship_to_state" value={ship_to_state} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>Postal Code :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="number" id="ship_to_postal_code" value={ship_to_postal_code} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>Email Address :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="email" id="ship_to_emailaddress" value={ship_to_emailaddress} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col sm={{ size: 3 }}>
                                    <Label>Phone :</Label>
                                </Col>
                                <Col sm={{ size: 9 }}>
                                    <Input type="number" id="ship_to_phonenum" value={ship_to_phonenum} onChange={this.handleChange} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col>
                                    <Button type="submit" disabled={!this.validateForm()} className="float-right">Submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Ship