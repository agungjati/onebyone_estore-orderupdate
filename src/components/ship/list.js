import React, { Component } from 'react';
import { Container, Row, Col, Table }
    from 'reactstrap'
import PropTypes from 'prop-types';
import axios from 'axios'

import { HeaderApi, UrlApi } from '../../headers'

class List extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: null,
            error: null,
            Loading: true
            
        }

    }


    componentDidMount() {
        axios({
            method: "GET",
            url: `${UrlApi}/update_order`,
            headers: HeaderApi
        }).then(response => {
            this.setState({
                data: response.data,
                Loading: false
            })
           
        }, error => {
            this.setState({
                Loading: false,
                error
            })

        })
        console.log(this.state)
    }

    render() {
        return (
            <Container className="bg-light">
                <Row className="bg-white shadow">
                    <Col md={{ size: 12 }} className="border-bottom py-2"><h4 className="text-center">eStore Logistics Order Update</h4></Col>
                    <Col md={{ size: 12 }} className="p-3">
                        <Row className="mt-3">
                            <Col md={{ size: 9 }}>
                                <b >List of shipments (This would be a nested table and if multiple shipment exist, a particular shipment can be clicked to populate the details in the next table)</b>
                            </Col>
                        </Row>
                        <Row className="mt-3">
                            <Col md={{ size: 12 }}>
                                <Table size="sm" bordered responsive>
                                    <thead>
                                        <tr>
                                            <th>Company </th>
                                            <th>ERP</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Post Code</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <TdList dataList={this.state} />
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}

Table.propTypes = {
    size: PropTypes.string,
    bordered: PropTypes.bool,
    responsive: PropTypes.bool
}

const stringOrNumberProp = PropTypes.oneOfType([PropTypes.number, PropTypes.string]);
const columnProps = PropTypes.oneOfType([
    PropTypes.shape({
        size: PropTypes.oneOfType([PropTypes.bool, PropTypes.number, PropTypes.string]),
        offset: stringOrNumberProp
    })
]);

const TdList = ({ dataList }) => {
    const { data, error, Loading} = dataList
    console.log(dataList)
    if(error !== null)
        return (<tr><td>{error}</td></tr>)
    else if(Loading){
     return (<tr><td>Loading...</td></tr>)
    }else{
        return data.map((elem, index) => {
            return (<tr key={index}>
                <td>{elem.company}</td>
                <td></td>
                <td>&nbsp;</td>
                <td>SKU</td>
                <td>SKU DESC</td>
                <td>QTY</td>
                <td>QTY</td>
                <td>QTY</td>
                <td>QTY</td>
            </tr>)
        })
    }
    
}

Col.propTypes = {
    md: columnProps
}

export default List;