import React, { Component } from 'react'
import {
    Button, FormGroup, Input , Label, Form,
    Container, Col, Row
} from 'reactstrap'
import { HeaderApi, UrlApi } from '../../headers'
import axios from 'axios'

class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: "",
            password: "",
            duration: 0          
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        })
    } 

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0
    }

    handleSubmit = event => {
        event.preventDefault()
        axios({
            method: "POST",
            url: `${UrlApi}/users`,
            data: JSON.stringify(this.state),
            headers: HeaderApi
        }).then(response => {
            console.log(response.data)
        })
    }

    render() {
        const { email, password } = this.state

        return (
            <Container className="mt-3">
                <Row>
                    <Col md={{ size: 4, offset: 4 }}>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup >
                            <Label>Email</Label>
                            <Input  autoFocus type="email"  id="email" value={email} onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup >
                            <Label>Password</Label>
                            <Input id="password"  type="password" value={password} onChange={this.handleChange} />
                        </FormGroup>
                        <Button block type="submit" disabled={!this.validateForm()}>Login</Button>
                    </Form>
                    </Col>                 
                </Row>
            </Container>
        )
    }
}

export default Login