import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NotFound from './NotFound';
import Login from './components/user/login';
import Ship from './components/ship';
import List from './components/ship/list' 

ReactDOM.render(
    <Router>
        <div>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/login" component={Login} />
                <Route path="/ship" component={Ship} />
                <Route path="/list" component={List} />
                <Route component={NotFound} />
            </Switch>
        </div>
    </Router>
    , document.getElementById('root'));

